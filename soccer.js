const RESULT_VALUES = {
  w: 3,
  d: 1,
  l: 0
}

// This function accepts one argument, the result, which should be a string
// Acceptable values are 'w', 'l', or 'd'
const getPointsFromResult = function getPointsFromResult(result) {
  return RESULT_VALUES[result];
}

// Create getTotalPoints function which accepts a string of results
// including wins, draws, and losses i.e. 'wwdlw'
// Returns total number of points won
const getTotalPoints = function(resultString){

  // example: converts resultString 'wwdlw' to resultsArray ['w', 'w', 'd', 'l', 'w']
  const resultsArray = resultString.split('');

  let points = 0; 
  
  resultsArray.forEach(function(result){
    points += getPointsFromResult(result);
  });
  
  return points;
}

// Check getTotalPoints
console.log(getTotalPoints('wwdl')); // should equal 7

// create orderTeams function that accepts as many team objects as desired, 
// each argument is a team object in the format { name, results }
// i.e. {name: 'Sounders', results: 'wwlwdd'}
// Logs each entry to the console as "Team name: points"
// iffe to hide function variables from global scope
(function(){
  const orderTeams = function() {
    // convert array-like arguments to actual Array type
    const args = Array.from(arguments);

    args.forEach(function (team){
      const name = team.name;
      const points = getTotalPoints(team.results);

      console.log(`${name}: ${points}`);
    }) // forEach
  } // orderTeams()

  // Check orderTeams
  orderTeams(
    {name: 'Sounders', results: 'wwdl'},
    {name: 'Galaxy', results: 'wlld'}
  ); 
  // should log the following to the console:
  // Sounders: 7
  // Galaxy: 4
})(); // iife
